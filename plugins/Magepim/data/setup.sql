DROP TABLE IF EXISTS `plugin_magepim`;
CREATE TABLE `plugin_magepim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteUrl` varchar(125) DEFAULT NULL,
  `consumerSecret` varchar(125) DEFAULT NULL,
  `consumerKey` varchar(125) DEFAULT NULL,
  `accessToken` mediumtext,
  `className` varchar(125) NOT NULL,
  `title` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `plugin_magepim` (className) VALUES ('Magepim_Consumer');
DROP TABLE IF EXISTS `magepim_products`;
CREATE TABLE `magepim_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sku` varchar(125) DEFAULT NULL,
  `type_id` varchar(125) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `price` decimal(12,4) NOT NULL default '0.0000',
  `name` varchar(125) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
