

pimcore.registerNS("magepim.product.tree");
magepim.product.tree = Class.create(pimcore.plugin.admin,{

    treeDataUrl: "/plugin/Magepim/admin/tree-get-childs-by-id/",
    getClassName: function() {
        return "magepim.product.tree";
    },
    
    pimcoreReady: function (params,broker){   
        
        //if (!config) {
            this.config = {
                rootId: 1,
                rootVisible: true,
                loaderBaseParams: {},
                treeId: "pimcore_panel_tree_products",
                treeIconCls: "pimcore_icon_product",
                treeTitle: t('products'),
                parentPanel: Ext.getCmp("pimcore_panel_tree_left"),
                index: 4
            };
        /*}
        else {
            this.config = config;
        }*/
        
        pimcore.layout.treepanelmanager.register(this.config.treeId);
        
        // get root node config
        Ext.Ajax.request({
            url: "/plugin/Magepim/admin/tree-get-root",
            params: {
                id: this.config.rootId
            },
            success: function (response) {
                var res = Ext.decode(response.responseText);
                var callback = function () {};
                if(res["id"]) {
                    callback = this.init.bind(this, res);
                }
                pimcore.layout.treepanelmanager.initPanel(this.config.treeId, callback);
            }.bind(this) 
        });
         
         
    },

    initialize: function(config) {
         pimcore.plugin.broker.registerPlugin(this);

        
    },
        init: function(rootNodeConfig) {

        rootNodeConfig.nodeType = "async";
        rootNodeConfig.text = "home";
        rootNodeConfig.draggable = false;
        rootNodeConfig.iconCls = "pimcore_icon_home";
        rootNodeConfig.listeners = this.getTreeNodeListeners();

        // assets
        this.tree = new Ext.tree.TreePanel({
            id: this.config.treeId,
            title: this.config.treeTitle,
            iconCls: this.config.treeIconCls,
            useArrows:true,
            autoScroll:true,
            animate:true,
            enableDD:true,
            ddGroup: "element",
            containerScroll: true,
            ddAppendOnly: true,
            rootVisible: this.config.rootVisible,
            forceLayout: true,            
            border: false,
            tools: [{
                id: "right",
                handler: pimcore.layout.treepanelmanager.toRight.bind(this)
            },{
                id: "left",
                handler: pimcore.layout.treepanelmanager.toLeft.bind(this),
                hidden: true
            }],
            root: rootNodeConfig,
            plugins: new Ext.ux.tree.TreeNodeMouseoverPlugin(),
            loader: new Ext.ux.tree.PagingTreeLoader({
                dataUrl:this.treeDataUrl,
                pageSize:50,
                enableTextPaging:false,
                pagingModel:'remote',
                requestMethod: "GET",
                baseAttrs: {
                    listeners: this.getTreeNodeListeners(),
                    reference: this,
                    allowDrop: true,
                    allowChildren: true,
                    isTarget: true,
                    nodeType: "async"
                },
                baseParams: this.config.loaderBaseParams,
            })
           
        });

        new Ext.tree.TreeSorter(this.tree, {folderSort:true});
        
        this.tree.on("startdrag", this.onDragStart.bind(this));
        this.tree.on("enddrag", this.onDragEnd.bind(this));
        this.tree.on("render", function () {
            this.getRootNode().expand();
        });
        this.tree.on("nodedragover", this.onTreeNodeOver.bind(this));
        this.tree.on("afterrender", function () {
            this.tree.loadMask = new Ext.LoadMask(this.tree.getEl(), {msg: t("please_wait")});
            this.tree.loadMask.enable();
        }.bind(this));
        

        this.config.parentPanel.insert(this.config.index, this.tree);
        this.config.parentPanel.doLayout();
        
    },
       getTreeNodeListeners: function () {
        var treeNodeListeners = {
            'click' : this.onTreeNodeClick,
            "contextmenu": this.onTreeNodeContextmenu,
            "move": this.onTreeNodeMove,
            "beforemove": this.onTreeNodeBeforeMove
        };

        return treeNodeListeners;
    },
        onTreeNodeClick: function () {
        if(this.attributes.permissions.view) {
            pimcore.helpers.openAsset(this.id, this.attributes.type);
        }
    },
    
        onTreeNodeContextmenu: function () {
        this.select();

        var menu = new Ext.menu.Menu();

        if (this.attributes.type == "folder") {
            if (this.attributes.permissions.create) {
                menu.add(new Ext.menu.Item({
                    text: t('add_assets'),
                    iconCls: "pimcore_icon_asset_add",
                    hideOnClick: false,
                    menu: [{
                        text: t("upload_files"),
                        handler: this.attributes.reference.addAssets.bind(this),
                        iconCls: "pimcore_icon_upload_multiple"
                    },{
                        text: t("upload_compatibility_mode"),
                        handler: this.attributes.reference.addSingleAsset.bind(this),
                        iconCls: "pimcore_icon_upload_single"
                    },{
                        text: t("upload_zip"),
                        handler: this.attributes.reference.uploadZip.bind(this),
                        iconCls: "pimcore_icon_upload_zip"
                    },{
                        text: t("import_from_server"),
                        handler: this.attributes.reference.importFromServer.bind(this),
                        iconCls: "pimcore_icon_import_server"
                    }]
                }));

                menu.add(new Ext.menu.Item({
                    text: t('add_folder'),
                    iconCls: "pimcore_icon_folder_add",
                    handler: this.attributes.reference.addFolder.bind(this)
                }));

            }

            menu.add(new Ext.menu.Item({
                text: t('refresh'),
                iconCls: "pimcore_icon_reload",
                handler: this.attributes.reference.refresh.bind(this)
            }));
        }

        if (this.attributes.permissions.rename && this.id != 1 && !this.attributes.locked) {
            menu.add(new Ext.menu.Item({
                text: t('edit_filename'),
                iconCls: "pimcore_icon_edit_key",
                handler: this.attributes.reference.editAssetFilename.bind(this)
            }));
        }

        if (this.id != 1) {
            menu.add(new Ext.menu.Item({
                text: t('copy'),
                iconCls: "pimcore_icon_copy",
                handler: this.attributes.reference.copy.bind(this)
            }));
        }

        //cut
        if (this.id != 1 && !this.attributes.locked) {
            menu.add(new Ext.menu.Item({
                text: t('cut'),
                iconCls: "pimcore_icon_cut",
                handler: this.attributes.reference.cut.bind(this)
            }));
        }


        //paste
        if (this.attributes.reference.cacheDocumentId && (this.attributes.permissions.create || this.attributes.permissions.publish)) {
            var pasteMenu = [];

            if (this.attributes.type == "folder") {
                menu.add(new Ext.menu.Item({
                    text: t('paste'),
                    iconCls: "pimcore_icon_paste",
                    handler: this.attributes.reference.pasteInfo.bind(this, "recursive")
                }));
            }
            else {
                menu.add(new Ext.menu.Item({
                    text: t('paste'),
                    iconCls: "pimcore_icon_paste",
                    handler: this.attributes.reference.pasteInfo.bind(this, "replace")
                }));
            }
        }

        if (this.attributes.type == "folder" && this.attributes.reference.cutAsset && (this.attributes.permissions.create || this.attributes.permissions.publish)) {
            menu.add(new Ext.menu.Item({
                text: t('paste_cut_element'),
                iconCls: "pimcore_icon_paste",
                handler: function() {
                    this.attributes.reference.pasteCutAsset(this.attributes.reference.cutAsset, this.attributes.reference.cutParentNode, this, this.attributes.reference.tree);
                    this.attributes.reference.cutParentNode = null;
                    this.attributes.reference.cutAsset = null;
                }.bind(this)
            }));
        }

        if (this.attributes.permissions.remove && this.attributes.id != 1 && !this.attributes.locked) {
            menu.add(new Ext.menu.Item({
                text: t('delete'),
                iconCls: "pimcore_icon_delete",
                handler: this.attributes.reference.deleteAsset.bind(this)
            }));
        }
        
        if (this.id != 1) {
            var user = pimcore.globalmanager.get("user");
            if(user.admin) { // only admins are allowed to change locks in frontend
                
                var lockMenu = [];
                if(this.attributes.lockOwner) { // add unlock
                    lockMenu.push({
                        text: t('unlock'),
                        iconCls: "pimcore_icon_lock_delete",
                        handler: function () {
                            this.attributes.reference.updateAsset(this.attributes.id, {locked: null}, function () {
                                this.attributes.reference.tree.getRootNode().reload();
                            }.bind(this))
                        }.bind(this)
                    });
                } else {
                    lockMenu.push({
                        text: t('lock'),
                        iconCls: "pimcore_icon_lock_add",
                        handler: function () {
                            this.attributes.reference.updateAsset(this.attributes.id, {locked: "self"}, function () {
                                this.attributes.reference.tree.getRootNode().reload();
                            }.bind(this))
                        }.bind(this)
                    });
                    
                    if(this.attributes.type == "folder") {
                        lockMenu.push({
                            text: t('lock_and_propagate_to_childs'),
                            iconCls: "pimcore_icon_lock_add_propagate",
                            handler: function () {
                                this.attributes.reference.updateAsset(this.attributes.id, {locked: "propagate"}, function () {
                                    this.attributes.reference.tree.getRootNode().reload();
                                }.bind(this))
                            }.bind(this)
                        });
                    }
                }
                
                menu.add(new Ext.menu.Item({
                    text: t('lock'),
                    iconCls: "pimcore_icon_lock",
                    hideOnClick: false,
                    menu:lockMenu
                }));
            }
        }

        menu.show(this.ui.getAnchor());
    },
    
    onTreeNodeBeforeMove: function (tree, element, oldParent, newParent, index) {
        
        // check for locks
        if (element.attributes.locked) {
            Ext.MessageBox.alert(t('locked'), t('element_cannot_be_move_because_it_is_locked'));
            return false;
        }

         // check new parent's permission
        if(!newParent.attributes.permissions.create){
            Ext.MessageBox.alert(t('missing_permission'), t('element_cannot_be_moved'));
            return false;
        }

        // check for permission
        if (element.attributes.permissions.settings) {
            tree.loadMask.show();
            return true;
        }
        return false;
    },
    
        onTreeNodeOver: function (event) {

        // check for permission
        try {
            if (event.data.node.attributes.permissions.settings) {
                return true;
            }
        }
        catch (e) {
        }

        return false;
    },
    onDragStart : function () {
        pimcore.helpers.dndMaskFrames();
    },

    onDragEnd : function () {
        pimcore.helpers.dndUnmaskFrames();
    },
    
   
   

    onTreeNodeMove: function (tree, element, oldParent, newParent, index) {

        this.attributes.reference.updateAsset(this.id, {
            parentId: newParent.id
        }, function (newParent, oldParent, tree, response) {
            try{
                var rdata = Ext.decode(response.responseText);
                if (rdata && rdata.success) {
                    // set new pathes
                    var newBasePath = newParent.attributes.path;
                    if (newBasePath == "/") {
                        newBasePath = "";
                    }
                    this.attributes.basePath = newBasePath;
                    this.attributes.path = this.attributes.basePath + "/" + this.attributes.text;
                }
                else {
                    tree.loadMask.hide();
                    pimcore.helpers.showNotification(t("error"), t("cant_move_node_to_target"), "error",t(rdata.message));
                    oldParent.reload();
                    newParent.reload();
                }
            } catch(e){
                 tree.loadMask.hide();
                 pimcore.helpers.showNotification(t("error"), t("cant_move_node_to_target"), "error");
                 oldParent.reload();
                 newParent.reload();
            }
            tree.loadMask.hide();

        }.bind(this, newParent, oldParent, tree));
    },

   

    
    copy: function () {
        this.attributes.reference.cacheDocumentId = this.id;
    },

    cut: function () {
        this.attributes.reference.cutAsset = this;
        this.attributes.reference.cutParentNode = this.parentNode;
    },

    pasteCutAsset: function(asset, oldParent, newParent, tree) {
        asset.attributes.reference.updateAsset(asset.id, {
            parentId: newParent.id
        }, function (newParent, oldParent, tree, response) {
            try{
                var rdata = Ext.decode(response.responseText);
                if (rdata && rdata.success) {
                    // set new pathes
                    var newBasePath = newParent.attributes.path;
                    if (newBasePath == "/") {
                        newBasePath = "";
                    }
                    this.attributes.basePath = newBasePath;
                    this.attributes.path = this.attributes.basePath + "/" + this.attributes.text;
                }
                else {
                    tree.loadMask.hide();
                    pimcore.helpers.showNotification(t("error"), t("cant_move_node_to_target"), "error",t(rdata.message));
                }
            } catch(e){
                 tree.loadMask.hide();
                 pimcore.helpers.showNotification(t("error"), t("cant_move_node_to_target"), "error");
            }
            tree.loadMask.hide();
             oldParent.reload();
             newParent.reload();
        }.bind(asset, newParent, oldParent, tree));

    },

    pasteInfo: function (type) {
        //this.attributes.reference.tree.loadMask.show();

        pimcore.helpers.addTreeNodeLoadingIndicator("product", this.id);

        Ext.Ajax.request({
            url: "/plugin/Magepim/admin/copy-info/",
            params: {
                targetId: this.id,
                sourceId: this.attributes.reference.cacheDocumentId,
                type: type
            },
            success: this.attributes.reference.paste.bind(this)
        });
    },

    paste: function (response) {

        try {
            var res = Ext.decode(response.responseText);

            if (res.pastejobs) {

                this.pasteProgressBar = new Ext.ProgressBar({
                    text: t('initializing')
                });

                this.pasteWindow = new Ext.Window({
                    title: t("paste"),
                    layout:'fit',
                    width:500,
                    bodyStyle: "padding: 10px;",
                    closable:false,
                    plain: true,
                    modal: true,
                    items: [this.pasteProgressBar]
                });

                this.pasteWindow.show();


                var pj = new pimcore.tool.paralleljobs({
                    success: function () {

                        try {
                            this.attributes.reference.pasteComplete(this);
                        } catch(e) {
                            console.log(e);
                            pimcore.helpers.showNotification(t("error"), t("error_pasting_asset"), "error");
                            this.parentNode.reload();
                        }
                    }.bind(this),
                    update: function (currentStep, steps, percent) {
                        if(this.pasteProgressBar) {
                            var status = currentStep / steps;
                            this.pasteProgressBar.updateProgress(status, percent + "%");
                        }
                    }.bind(this),
                    failure: function (message) {
                        this.pasteWindow.close();
                        this.pasteProgressBar = null;

                        pimcore.helpers.showNotification(t("error"), t("error_pasting_asset"), "error", t(message));
                        this.parentNode.reload();
                    }.bind(this),
                    jobs: res.pastejobs
                });
            } else {
                throw "There are no pasting jobs";
            }
        } catch (e) {
            console.log(e);
            Ext.MessageBox.alert(t('error'), e);
            this.attributes.reference.pasteComplete(this);
        }
    },

    pasteComplete: function (node) {
        if(node.pasteWindow) {
            node.pasteWindow.close();
        }

        node.pasteProgressBar = null;
        node.pasteWindow = null;

        //this.tree.loadMask.hide();
        pimcore.helpers.removeTreeNodeLoadingIndicator("asset", node.id);
        node.reload();
    },



    refresh: function () {
        this.reload();
    },

    addFolder : function () {
        Ext.MessageBox.prompt(t('add_folder'), t('please_enter_the_name_of_the_folder'), this.attributes.reference.addFolderCreate.bind(this));
    },

    addFolderCreate: function (button, value, object) {

        if (button == "ok") {
            Ext.Ajax.request({
                url: "/plugin/Magepim/admin/add-folder/",
                params: {
                    parentId: this.id,
                    name: pimcore.helpers.getValidFilename(value)
                },
                success: this.attributes.reference.addFolderComplete.bind(this)
            });
        }
    },

    addFolderComplete: function (response) {
        try{
            var rdata = Ext.decode(response.responseText);
            if (rdata && rdata.success) {
                this.leaf = false;
                this.renderIndent();
                this.expand();
            }
            else {
                pimcore.helpers.showNotification(t("error"), t("there_was_a_problem_creating_a_folder"), "error",t(rdata.message));
            }
        } catch(e){
            pimcore.helpers.showNotification(t("error"), t("there_was_a_problem_creating_a_folder"), "error");
        }
        this.reload();
    }



    
});

    layoutProductTree = new magepim.product.tree();
    pimcore.globalmanager.add("layout_product_tree", layoutProductTree);
