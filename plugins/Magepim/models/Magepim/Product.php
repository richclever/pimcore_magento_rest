<?php
class Magepim_Product extends Pimcore_Model_Abstract {

    public $id;
    public $entity_id;
    public $sku;
    public $type_id;
    public $status;
    public $price;
    public $name;

    public function save() {
        return $this->getResource()->save();
    }

    public static function getById($id) {
        $obj = new self;
        $obj->getResource()->getById($id);
        return $obj;
    }
    public static function getByEntityId($id) {
        $obj = new self;
        $obj->getResource()->getByEntityId($id);
        return $obj;
    }
    public static function getAllProducts(){
        $obj = new self;
        $products = $obj->getResource()->getAllProducts();
        return $products;
    }
    


    // just getters/setters
    public function setEntity_id($entityId) {
        $this->entity_id = $entityId;
    }
    
    public function getEntity_id() {
        return $this->entity_id;
    }
    public function setSku($sku) {
        $this->sku= $sku;
    }

    public function getSku() {
        return $this->sku;
    }
    
    public function setType_id($typeId) {
        $this->type_id = $typeId;
    }
    
    public function getType_id() {
        return $this->type_id;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getStatus() {
        return $this->status;
    }
    
    public function setPrice($price){
        $this->price = $price;
    }
    public function getPrice(){
        return $this->price;
    }
    public function setName($name){
        $this->name = $name;
    }
    public function getName(){
        return $this->name;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }
    
    public function hasChilds() {
        if(is_bool($this->hasChilds)){
            if(($this->hasChilds and empty($this->childs)) or (!$this->hasChilds and !empty($this->childs))){
                return $this->getResource()->hasChilds();
            } else {
                return $this->hasChilds;
            }
        }
        return $this->getResource()->hasChilds();
    }
    
}