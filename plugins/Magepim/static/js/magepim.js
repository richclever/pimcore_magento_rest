pimcore.registerNS("magepim.magepim");

magepim.magepim = Class.create({

    initialize: function(){
        this.createPanel();
    },

    

    getData: function(){
    	     Ext.Ajax.request({
            url: "/plugin/Magepim/magento/products",
            params: {
                xaction: "read"
            },
            success: this.getDataComplete.bind(this)
        });
    },

    getDataComplete: function(response){
        try{
            var data = Ext.decode(response.responseText);
            this.createPanel(data);
        }
        catch(e){
            console.log(e);
        }
    },

    createPanel: function(){
        
        var tabPanel = Ext.getCmp("pimcore_panel_tabs");

        var layout = this.getLayout();

        var panel = new Ext.Panel({
            id: "magepimBody",
            title: t("magepim"),
            border: true,
            layout:'fit',
            closable:true,
            iconCls:"magepim_icon",
            bodyStyle: 'padding:4px;',
            items:layout
        });



        tabPanel.add(panel);
        tabPanel.activate("magepimBody");

    },

    getLayout: function(){
        var users = this.getUsers();
        var letters = this.getLetters();
        var layout = new Ext.TabPanel({        
            activeTab: 0,
            items:[letters,users]
        });
        return layout;
    },
/////////////////////////////////////////////////////////////////////
//Paneau Users
    onActive : function(btn, ev) {        
        var id = this.ugrid.getSelectionModel().getSelected().get('id');
        Ext.Ajax.addListener('requestcomplete', this.recharge,this);

        Ext.Ajax.request({
            url: '/plugin/Magepim/admin/activeuser',
            headers: {
                'action': 'desactive'
            },
            params: {
                id: id,
                xaction:"active"
            }
        });

    },  

    recharge: function(){
        Ext.Ajax.removeListener('requestcomplete', this.recharge,this);
        this.ugrid.store.reload();
        
        
    },
    /**
     * onDelete
     */
    onDesactive : function(btn, ev) {
        var id = this.ugrid.getSelectionModel().getSelected().get('id');
        Ext.Ajax.addListener('requestcomplete', this.recharge,this);

        Ext.Ajax.request({
            url: '/plugin/Magepim/admin/activeuser',
            headers: {
                'action': 'desactive'
            },
            params: {
                id: id,
                xaction:"desactive"
            }
        });
    },

    
    getUsers: function(){



        var expander = new Ext.ux.grid.RowExpander({
        tpl : new Ext.Template('{meta}')
        });

        function actifR(val) {
            if (val== 1) {
                return '<img src="/pimcore/static/img/icon/status_online.png"/>';
            } else{
                return '<img src="/pimcore/static/img/icon/status_offline.png"/>';
            }
            return val;
        }

        var proxy = new Ext.data.HttpProxy({
            url: '/plugin/Magepim/admin/users'
        });

       

        var readerFields = [
        {
            name: 'id'
        },

        {
            name: 'email'
        },

        {
            name: 'type'
        },

        {
            name: 'actif'
        },
        {
            name: 'meta'
        }
        ,
        {
            name: 'date'
        }
        ];
        var reader = new Ext.data.JsonReader({
            totalProperty: 'total',
            successProperty: 'success',
            root: 'data',
            idProperty: 'id'
        }, readerFields);

        var writer = new Ext.data.JsonWriter();
        // create the data store
        this.ustore = new Ext.data.Store({
            fields: readerFields,
            root:'data',
            restful: false,
            proxy: proxy,
            reader: reader,
            writer: writer
        });
        
        // create the Grid
        this.ugrid = new Ext.grid.GridPanel({
            store: this.ustore,
            plugins: expander,
            tbar:
            [{
                text: 'Activer',
                icon: '/pimcore/static/img/icon/status_online.png',
                handler: this.onActive,
                scope: this
            }, '-', {
                text: 'Desactiver',
                icon: '/pimcore/static/img/icon/status_offline.png',
                handler: this.onDesactive,
                scope: this
            }, '-'],
            columns: [expander,
            {
                id       :'id',
                header   : 'id',
                width    : 75,
                sortable : true,
                dataIndex: 'id',
                hidden:true
            },

            {
                header   : 'Actif',
                width    : 50,
                sortable : true,
                dataIndex: 'actif',
                renderer:actifR
            },
            {
                id:'email',
                header   : 'E-mail',
                width    : 150,
                sortable : true,
                dataIndex: 'email'
            },
            {
                header   : 'Groupe',
                width    : 150,
                sortable : true,
                dataIndex: 'type'
            },
            {
                header   : "Date d'inscription",
                width    : 150,
                sortable : true,
                dataIndex: 'date',
                renderer: function(d) {
                        var date = new Date(d * 1000);
                        return date.format("d/m/Y H:i:s");
                    }
            }
            ],
            stripeRows: true,
            autoExpandColumn: 'email',
            height: 350,
            width: 600,
            iconCls : "subscriber_icon",
            title: t('users'),
            // config options for stateful behavior
            id:'userGrid'
        });
        
        this.ustore.load();
        return this.ugrid;
    },

    onActive : function(btn, ev) {
        var id = this.ugrid.getSelectionModel().getSelected().get('id');
        Ext.Ajax.addListener('requestcomplete', this.recharge,this);

        Ext.Ajax.request({
            url: '/plugin/Magepim/admin/activeuser',
            headers: {
                'action': 'desactive'
            },
            params: {
                id: id,
                xaction:"active"
            }
        });

    },
////////////////////////////////////////////////////////////////////////////////////////
//Panneau newsletter

    getLetters: function(){

        var expander = new Ext.ux.grid.RowExpander({
        tpl : new Ext.Template('<p><b>Sujet:</b><br/><div style="color:#666666;"> {subject}</div></p><br>',
            '<p><b>Texte:</b><br/><div style="color:#666666;"> {text}</div></p>')
    });
        var group = new Ext.grid.GroupingView({
            forceFit:true,
            groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "lettres" : "lettres"]})'
        });

        
        var proxy = new Ext.data.HttpProxy({
            url: '/plugin/Magepim/admin/magento'
        });



        var readerFields = [
        {
            name: 'id'
        },

        {
            name: 'docid'
        },

        {
            name: 'subject'
        },

        {
            name: 'text'
        },

        {
            name: 'type'
        },

        {
            name: 'date'
        }
        ];
        var reader = new Ext.data.JsonReader({
            totalProperty: 'total',
            successProperty: 'success',
            root: 'data',
            idProperty: 'id'
        }, readerFields);

        var writer = new Ext.data.JsonWriter();
        // create the data store
        this.nstore = new Ext.data.GroupingStore({
            fields: readerFields,
            root:'data',
            restful: false,
            proxy: proxy,
            reader: reader,
            writer: writer,
            sortInfo:{field: 'type', direction: "ASC"},
            groupField:'type'
        });

        // create the Grid
        this.ngrid = new Ext.grid.GridPanel({
            store: this.nstore,
            plugins: expander,
            view: group,            
            columns: [expander,
            {
                id       :'id',
                header   : 'id',
                width    : 75,
                sortable : true,
                dataIndex: 'id',
                hidden:true
            },
            {
                id:'docid',
                header   : 'Magento ID',
                width    : 20,
                sortable : true,
                dataIndex: 'docid',
                
            },
            {
                id:'sku',
                header   : t('sku'),
                width    : 150,
                sortable : true,
                dataIndex: 'subject'
            },
            {
                header   : 'Groupe',
                width    : 50,
                sortable : true,
                dataIndex: 'type'
            },
            {
                header   : "Date d'envoi",
                width    : 150,
                sortable : true,
                dataIndex: 'date',
                renderer: function(d) {
                        var date = new Date(d * 1000);
                        return date.format("d/m/Y H:i:s");
               }
            }
            ],
            stripeRows: true,
            autoExpandColumn: 'subject',
            height: 350,
            width: 600,
            iconCls:"magepim_icon",
            title: t('magentoProducts'),
            // config options for stateful behavior
            id:'magepimrGrid'
        });

        this.nstore.load();
        return this.ngrid;
    }



});
