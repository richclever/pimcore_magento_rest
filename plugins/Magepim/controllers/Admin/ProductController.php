<?php
class Admin_ProductController extends Pimcore_Controller_Action_Admin {
    
    public function treeGetRootAction() {
    
        $id = 1;
        if ($this->_getParam("id")) {
            $id = intval($this->_getParam("id"));
        }
    
        $root = Magepim_Product::getById($id);
        if ($root->isAllowed("list")) {
            $this->_helper->json($this->getTreeNodeConfig($root));
        }
    
        $this->_helper->json(array("success" => false, "message" => "missing_permission"));
    }
}