<?php

class Magepim_AdminController extends Pimcore_Controller_Action_Admin {

   public function configAction(){
       $consumers = Magepim_Plugin::getConsumers();
       $viewConsumers = new ArrayObject();
       
       foreach($consumers as $consumer) {
           $hasAccess = false;
           $consumerView = array();
       
           $class = 'Magepim_Consumer';
           $config = array(
                   'consumerSite'    => $consumer['siteUrl'],
                   'consumerKey'		=> $consumer['consumerKey'],
                   'consumerSecret'	=> $consumer['consumerSecret'],
                   'accessToken'		=> unserialize($consumer['accessToken']),
                   'consumer'			=> $class
           );
       
           if($config['accessToken'] instanceof Zend_Oauth_Token_Access) {
               $hasAccess = true;
           }
       
           $form = new Magepim_Form_Credentials();
           $form->setDefaults($config);
           $form->setName($class);
           $form->setAttrib('class', 'consumer-form');
           $form->setIsArray(true);
       
           $consumerClass = new $class();
           $consumerModel = new Magepim_Consumer();
           $consumerModel->setConsumer($consumerClass)
           ->setForm($form)
           ->setName(str_replace('_', ' ', $class))
           ->setHasAccess($hasAccess);
       
           $viewConsumers->append($consumerModel);
    }
       $this->view->consumers = $viewConsumers;
   }
    public function savesettingAction(){
        $this->removeViewRenderer();
        $datas = $this->_getParam('data');
        $data = Zend_Json::decode($datas);
        $data['installed']=1;

        $config = new Zend_Config($data, true);
        $writer = new Zend_Config_Writer_Xml(array(
                    "config" => $config,
                    "filename" => PIMCORE_PLUGINS_PATH . "/Magepim/config.xml"
                ));
        $writer->write();


        $this->_helper->json(array("success" => true));
    }

    public function productsAction(){
        $config = new Zend_Config_Xml(PIMCORE_PLUGINS_PATH . "/Magepim/config.xml");

        $data = $config->toArray();
        
        $this->_helper->json($data);

    }

        
    /**
     * Get Accesstoken from service provider
     */
    public function getAccessAction() {
    
        $consumer = $this->_getParam('consumer');
    
        try {
            /* @var $consumerModel OAuth_Interface_Consumer */
            $consumerModel = new $consumer();
            $oAuthHandler = new Magepim_Handler($consumerModel);
            $oAuthHandler->connect();
        } catch(Exception $e) {
            $this->view->error = $e->getMessage();
        }
    }
    /**
     * When service provider redirects back with the access token
     * it will be stored here
     */
    public function addTokenAction() {
        $consumer = $this->_getParam('consumer');
        /* @var $consumerModel OAuth_Interface_Consumer */
        $consumerModel = new $consumer();
        try {
            $oAuthHandler = new Magepim_Handler($consumerModel);
            $oAuthHandler->handleAccessToken($_GET);
    
            $this->_forward('config');
        } catch (Exception $e) {
    
        }
    }
    /**
     * Stores consumer key and secret
     */
    public function addKeysAction() {
    
        $form = new Magepim_Form_Credentials();
        $consumer = $this->_getParam('consumer');
        if($this->_request->isPost()) {
            if($form->isValid($_POST)) {
                $values = $form->getValues();
                $updateValues = array(
                        'siteUrl' => $values['consumerSite'],
                        'consumerKey' => $values['consumerKey'],
                        'consumerSecret' => $values['consumerSecret']
                );
                $rows = Magepim_Plugin::updateConsumerConfig(
                        $consumer,
                        $updateValues
                );
                $return = array(
                        'success' => 'Credentials updated successfully '.$consumer
                );
                $this->_helper->json($return);
                return;
            } else {
                $errors = array();
                foreach($form->getErrors() as $error => $reason) {
                    if($reason) {
                        $errors[] = $error;
                    }
                }
                $this->_helper->json(array('errors' => $errors));
                return;
            }
        }
        $this->view->form = $form;
    }
    /**
     * Removes access token which means consumer is disconnected
     * from the service provider
     */
    public function disconnectAction() {
        $consumer = $this->_getParam('consumer');
    
        try {
            Magepim_Plugin::updateConsumerConfig($consumer, array('accessToken' => null));
            $this->_helper->json(array('success' => 'disconnected'));
        } catch (Exception $e) {
            $this->_helper->json(array('error' => $e->getMessage()));
        }
    }
    
    /**
     * Are consumerKey and secret already entered and stored?
     */
    public function hasKeysAction() {
        $return = array('hasKeys' => false);
        $consumer = $this->_getParam('consumer');
    
        $row = Magepim_Plugin::getConsumerConfig($consumer);
        if($row['consumerKey'] && $row['consumerSecret']) {
            $return['hasKeys'] = true;
        }
        $this->_helper->json($return);
    }
    
    public function treeGetProductsAction() {
    
        $id = 1;
        if ($this->_getParam("id")) {
            $id = intval($this->_getParam("id"));
        }
    
        $root = Magepim_Product::getAllProducts();
        //if ($root->isAllowed("list")) {
            $this->_helper->json($this->getTreeNodeConfig($root));
        //}
    
        $this->_helper->json(array("success" => false, "message" => "missing_permission"));
    }
protected function getTreeNodeConfig($product)
    {
    
       $tmpProduct = array(
            "id" => $product->getId(),
            "text" => $product->getName(),
            "type" => $product->getType_Id(),
            "sku" => $product->getSku(),
            "elementType" => "product",
            "leaf" => true
       );



        
        return $tmpProduct;
    }
    public function treeGetRootAction() {
    
        $id = 1;
        if ($this->_getParam("id")) {
            $id = intval($this->_getParam("id"));
        }
    
        $root = Magepim_Product::getById($id);
        //if ($root->isAllowed("list")) {
            $this->_helper->json($this->getTreeNodeConfig($root));
        //}
    
        $this->_helper->json(array("success" => false, "message" => "missing_permission"));
    }
    
    
    public function treeGetChildsByIdAction() {

        $product = Magepim_Product::getById($this->_getParam("node"));

        $products = array();
        if ($product->hasChilds()) {
            $limit = intval($this->_getParam("limit"));
            if (!$this->_getParam("limit")) {
                $limit = 100000000;
            }
            $offset = intval($this->_getParam("start"));

            $list = new Magepim_Product_List();
            $list->setCondition("parentId = ?", $product->getId());
            $list->setOrderKey("index");
            $list->setOrder("asc");
            $list->setLimit($limit);
            $list->setOffset($offset);

            $childsList = $list->load();

            foreach ($childsList as $childProduct) {
                // only display document if listing is allowed for the current user
                //if ($childProduct->isAllowed("list")) {
                    $products[] = $this->getTreeNodeConfig($childProduct);
                //}
            }
        }

        if ($this->_getParam("limit")) {
            $this->_helper->json(array(
                "total" => $product->getChildAmount(),
                "nodes" => $products
            ));
        }
        else {
            $this->_helper->json($products);
        }

        $this->_helper->json(false);
    }
    
        public function addFolderAction() {

        $success = false;
        $parentAsset = Magepim_Product::getById(intval($this->_getParam("parentId")));
        $equalAsset = Magepim_Product::getByPath($parentAsset->getFullPath() . "/" . $this->_getParam("name"));

        if ($parentAsset->isAllowed("create")) {

            if (!$equalAsset) {
                $asset = Magepim_Product::create($this->_getParam("parentId"), array(
                    "filename" => $this->_getParam("name"),
                    "type" => "folder",
                    "userOwner" => $this->user->getId(),
                    "userModification" => $this->user->getId()
                ));
                $success = true;
            }
        }
        else {
            Logger::debug("prevented creating asset because of missing permissions");
        }

        $this->_helper->json(array("success" => $success));
    }


}

