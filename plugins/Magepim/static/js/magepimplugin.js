pimcore.registerNS("magepim.settings");

pimcore.plugin.magepim = Class.create(pimcore.plugin.admin, {

    getClassName: function () {
        return "magepim.settings";
    },

    initialize: function() {
        pimcore.plugin.broker.registerPlugin(this);
    },


    uninstall: function() {
        //TODO: hide all commenting tabs
    },
    
    pimcoreReady: function (params,broker){

        var toolbar = Ext.getCmp("pimcore_panel_toolbar");

       var action = new Ext.Action({
            text: t('setting_magepim'),
            iconCls:"magepim_icon",
            handler: function(){
              var settings = new magepim.settings;
            }
        });
        var user = pimcore.globalmanager.get("user");
        if (user.admin == true){

            toolbar.items.items[2].menu.add(action);
        }
        settings.getLayout();

       
    }, 
    
    getProducts:function(){
        Ext.Ajax.request({
            url: '/plugin/Magepim/magento/products',
            scope:this,
            success: function (response) {
                var resp = Ext.util.JSON.decode(response.responseText);                
                pimcore.globalmanager.add("Magepim.doctype",resp.DoctypeNewsletter);
            }
        });
    },
    

    postOpenObject: function(object, type) {

        var items = object.tab.items.items[1].items;
        var commenttab = new magepim.settings(object, type);
        object.tab.items.items[1].insert(object.tab.items.items[1].items.length, commenttab.getLayout());
        object.tab.items.items[1].doLayout();
        pimcore.layout.refresh();

    },
    postOpenDocument: function(object, type) {

        var items = object.tab.items.items[1].items;
        var commenttab = new magepim.settings(object, type);
        object.tab.items.items[1].insert(object.tab.items.items[1].items.length, commenttab.getLayout());
        object.tab.items.items[1].doLayout();
        pimcore.layout.refresh();

    },
    postOpenAsset: function(object, type) {

        var items = object.tab.items.items[1].items;
        var commenttab = new magepim.settings(object, type);
        object.tab.items.items[1].insert(object.tab.items.items[1].items.length, commenttab.getLayout());
        object.tab.items.items[1].doLayout();
        pimcore.layout.refresh();

    }

});

new pimcore.plugin.magepim();
