<?php
/**
 * Pimcore
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.pimcore.org/license
 *
 * @category   Pimcore
 * @package    Document
 * @copyright  Copyright (c) 2009-2010 elements.at New Media Solutions GmbH (http://www.elements.at)
 * @license    http://www.pimcore.org/license     New BSD License
 */

class Magepim_Product_List_Resource extends Pimcore_Model_List_Resource_Abstract {
    
    protected $tableName = 'magepim_products';

    /**
     * Loads a list of objects (all are an instance of Document) for the given parameters an return them
     *
     * @return array
     */
    public function load() {
        $products = array();
        $productsData = $this->db->fetchAll("SELECT id,type_id FROM ".$this->tableName . $this->getCondition() .  $this->getOffsetLimit(), $this->model->getConditionVariables());

        foreach ($productsData as $productData) {
            if($productData["type_id"]) {
                if($doc = Magepim_Product::getById($productData["id"])) {
                    $products[] = $doc;
                }
            }
        }

        $this->model->setProducts($products);
        return $products;
    }

    /**
     * Loads a list of document ids for the specicifies parameters, returns an array of ids
     *
     * @return array
     */
    public function loadIdList() {
        $documentIds = $this->db->fetchCol("SELECT id FROM " . $this->tableName  . $this->getCondition() . $this->getOffsetLimit(), $this->model->getConditionVariables());
        return $documentIds;
    }

    protected function getCondition() {
        if ($cond = $this->model->getCondition()) {
            if (Document::doHideUnpublished() && !$this->model->getUnpublished()) {
                return " WHERE (" . $cond . ") AND published = 1";
            }
            return " WHERE " . $cond . " ";
        }
        else if (Document::doHideUnpublished() && !$this->model->getUnpublished()) {
            return " WHERE published = 1";
        }
        return "";
    }

    public function getCount() {
        $amount = $this->db->fetchOne("SELECT COUNT(*) as amount FROM " .$this->tableName . $this->getCondition() . $this->getOffsetLimit(), $this->model->getConditionVariables());
        return $amount;
    }

    public function getTotalCount() {
        $amount = $this->db->fetchOne("SELECT COUNT(*) as amount FROM " .$this->tableName . $this->getCondition(), $this->model->getConditionVariables());
        return $amount;
    }
    
      public function getChildAmount() {
        $c = $this->db->fetchOne("SELECT COUNT(*) AS count FROM ".$this->tableName." WHERE parentId = ?", $this->model->getId());
        return $c;
    }
}
