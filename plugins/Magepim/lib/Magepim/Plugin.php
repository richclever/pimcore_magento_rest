<?php
class Magepim_Plugin extends Pimcore_API_Plugin_Abstract implements Pimcore_API_Plugin_Interface{
    /**
     *
     * @var Zend_Config_Xml
     */
    private static $config = null;
    
    const TABLE_NAME = 'plugin_magepim';
    /**
     * Initial config file
     */
    
public static function install(){
  $sql = file_get_contents(self::getPluginPath().'/data/setup.sql');
		$db = Pimcore_API_Plugin_Abstract::getDb();
		$db->exec($sql);
		if(self::isInstalled()) {
			return 'Magepim Magento connector installed and ready to be used.';
		}
		return 'Something went wrong.  Plugin could not be installed.  Check your logs!!!';
	}
	
	public static function uninstall(){
		/**
	 *
	 * @param OAuth_Interface_Consumer $consumer
	 */
	
		//$className = get_class($consumer);
		$db = Pimcore_API_Plugin_Abstract::getDb();
		$db->delete(self::TABLE_NAME, "className = 'Magepim_Consumer'");
	
				
	$message = 'Uninstall failed';		
		if(!self::getConsumers()) {
			$db = Pimcore_API_Plugin_Abstract::getDb();
			$db->exec('DROP TABLE '.self::TABLE_NAME);
			if(!self::isInstalled()) {
				$message =  'Magepim Magento connector is now uninstalled!';
			}
		} else {
			$message = 'Please uninstall all consumers first.';
		}
		return $message;
		
	}

/**
	 *
	 * @return bool
	 */
	public static function isInstalled() {
		$db = Pimcore_API_Plugin_Abstract::getDb();
		try {
			$db->describeTable(self::TABLE_NAME);
			return true;
		} catch(Zend_Db_Statement_Exception $e) {
			return false;
		}
		return false;
	}
	/**
	 *
	 * @return string
	 */
	public static function getPluginPath() {
	    return PIMCORE_PLUGINS_PATH.'/Magepim';
	}
	/**
	 *
	 * @return bool
	 */
	public static function readyForInstall() {
	    return is_writeable(self::getPluginPath());
	}
	
	
	public static function getConfig($reload = false) {
	    try {
	        if(!self::$config || $reload === true) {
	            self::$config = new Zend_Config_Xml(
	                    self::getConfigFileName(),
	                    null,
	                    true
	            );
	        }
	        return self::$config;
	    } catch (Zend_Config_Exception $e) {
	        Logger::alert($e->getMessage());
	    }
	    return null;
	}
	/**
	 *
	 * @param mixed $name array (key => value) or string
	 * @param mixed $value
	 */
	public static function updateConfig($name, $value = null) {
	    $config = self::getConfig();
	
	    if(is_array($name)) {
	        foreach($name as $key => $value) {
	            $config->{$key} = $value;
	        }
	    } else {
	        $config->{$name} = $value;
	    }
	
	    $writer = new Zend_Config_Writer_Xml(
	            array(
	                    'config'	=> $config,
	                    'filename'	=> self::getConfigFileName()
	            )
	    );
	    $writer->write();
	}
	/**
	 * Returns just the config filename
	 * @return string
	 */
	protected static function getConfigFileName() {
	    return PIMCORE_PLUGINS_PATH.'/Magepim/config.xml';
	}

	public static function getTranslationFileDirectory(){
        	return PIMCORE_PLUGINS_PATH."/Magepim/texts";
    	}
    	
    	/**
    	
    	*
    	
    	* @param string $language
    	
    	* @return string path to the translation file relative to plugin direcory
    	
    	*/
    	
    	public static function getTranslationFile($language) {
    	
    	    if ($language == "fr") {
    	
    	        return "/Magepim/texts/fr.csv";
    	
    	    } else {
    	
    	        return "/Magepim/texts/en.csv";
    	
    	    }
    	
    	}
    	
    	/**
    	 *
    	 * @return Zend_Db_Table_Rowset
    	 */
    	public static function getConsumers() {
    	    if(self::isInstalled()) {
    	        $db = Pimcore_API_Plugin_Abstract::getDb();
    	        return $db->fetchAll('SELECT * FROM '.self::TABLE_NAME.' ORDER BY id DESC');
    	    }
    	    return null;
    	}
    	
    	public static function getConsumer($consumer) {
    	    return new $consumer();
    	}
    	/**
    	 *
    	 * @param string $consumer
    	 * @param array $data
    	 */
    	public static function updateConsumerConfig($consumer, $data) {
    	    $db = Pimcore_API_Plugin_Abstract::getDb();
    	    return $db->update(self::TABLE_NAME, $data, "className = '$consumer'");
    	}
    	/**
    	 *
    	 * @param string $consumer
    	 * @return Zend_Db_Table_Row
    	 */
    	public static function getConsumerConfig($consumer) {
    	    $db = Pimcore_API_Plugin_Abstract::getDb();
    	    return $db->fetchRow("SELECT * FROM ".self::TABLE_NAME." WHERE className = '$consumer'");
    	}
    	/**
    	 *
    	 * @param OAuth_Interface_Consumer $consumer
    	 * @return OAuth_Handler
    	 */
    	public static function getHandler($consumer) {
    	    $oauthHandler = new Magepim_Handler($consumer);
    	    return $oauthHandler;
    	}

	
}