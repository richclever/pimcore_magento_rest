<?php

/**
 * OAuth consumer interface
 *
 * @author mk
 */
interface Magepim_Interface_Consumer {
	/**
	 * @return Zend_Config
	 */
	public function getConfig();
}

?>
