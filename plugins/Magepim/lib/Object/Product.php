<?php

class Object_Product extends Object_Concrete {

public $o_classId = 3;
public $o_className = "product";
public $id;
public $sku;
public $product_type;
public $status;
public $price;
public $proudct_name;
public $short_description;
public $description;

/**
* @param array $values
* @return Object_Module
*/
public static function create($values = array()) {
	$object = new self();
	$object->setValues($values);
	return $object;
}

public function getSku () {
    $preValue = $this->preGetValue("sku");
    if($preValue !== null && !Pimcore::inAdmin()) {
        return $preValue;
    }
    $data = $this->getClass()->getFieldDefinition("sku")->preGetData($this);
    return $data;
}
public function setSku ($sku) {
    $this->sku = $sku;
}
public function postAddObject(Object_Abstract $object) {
    $this->postAddElement($object);
}

}