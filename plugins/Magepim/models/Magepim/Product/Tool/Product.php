<?php
class Magepim_Product_Tool_Product extends Pimcore_Placeholder_Abstract
{

    public function getTestValue()
    {
        $value = '';
        switch ($this->getPlaceholderKey()) {
            case 'firstName' :
                $value = 'Homer';
                break;
            case 'lastName' :
                $value = 'Simpson';
                break;
            //...
        }
        return '<span class="testValue">' . $value . ' </span>';
    }

    public function getReplacement()
    {
        $value = $this->getValue();

        //$document would contain the document that we passed as third parameter to the replacePlaceholders() method
        $document = $this->getDocument();

        switch ($this->getPlaceholderKey()) {
            case 'firstName' :
                return ucfirst($value);
                break;
            case 'lastName'  :
                return ucfirst($value);
                break;
            case 'salutation' :
                if ($value == 'mr') {
                    return 'Dear mr.';
                } elseif ($value == 'mrs') {
                    return 'Dear mrs.';
                }
                break;
        }
    }
}
