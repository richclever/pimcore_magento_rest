pimcore.registerNS("pimcore.plugin.magepim");

pimcore.plugin.magepim = Class.create(pimcore.plugin.admin, {


    getClassName: function () {
        return "pimcore.plugin.magepim";
    },

    initialize: function() {
        pimcore.plugin.broker.registerPlugin(this);
    },


    uninstall: function() {
    //TODO: hide all commenting tabs
    },

    pimcoreReady: function (params,broker){

        var toolbar = Ext.getCmp("pimcore_panel_toolbar");

       var action = new Ext.Action({
            text: t('setting_magepim'),
            iconCls:"magepim_icon",
            handler: function(){
              var settings = new magepim.settings;
            }
        });
        var user = pimcore.globalmanager.get("user");
        if (user.admin == true){

            toolbar.items.items[2].menu.add(action);
        }
              

       this.getDoctypes();
    }, 

    getDoctypes:function(){
        Ext.Ajax.request({
            url: '/plugin/Magepim/magento/products',
            scope:this,
            success: function (response) {
                var resp = Ext.util.JSON.decode(response.responseText);                
                pimcore.globalmanager.add("Magepim.doctype",resp.DoctypeNewsletter);
            }
        });
    },

    postOpenObject: function(object, type) {

       

    },
    postOpenDocument: function(doc, type) {
        var localContAct = doc.data.controller + "/" + doc.data.action;
        var isOk = false;
        //var items = object.tab.items.items[1].items;
        var dtn = pimcore.globalmanager.get("Magepim.doctype");
        var docTypeNews = dtn.split(",");
        var storeDoctype = pimcore.globalmanager.store.document_types_store;
        for(var i=0;i<docTypeNews.length;i++){            
            var theDc = storeDoctype.getById(docTypeNews[i]);
            var ContAct = theDc.get('controller')+ "/" + theDc.get('action');
            if(ContAct == localContAct){
                isOk = true;
                break;
            }
        }
        if (isOk == true){
        var sendTab = new pimcore.plugin.magepim.sendPanel(doc, type);
        doc.tab.items.items[1].add(sendTab.getLayout());
        doc.tab.items.items[1].doLayout();
        pimcore.layout.refresh();
        }

    },
    postOpenAsset: function(object, type) {

     

    }

});

new pimcore.plugin.magepim();
