/**
 * Pimcore
 * 
 * LICENSE
 * 
 * This source file is subject to the new BSD license that is bundled with this
 * package in the file LICENSE.txt. It is also available through the
 * world-wide-web at this URL: http://www.pimcore.org/license
 * 
 * @copyright Copyright (c) 2009-2010 elements.at New Media Solutions GmbH
 *            (http://www.elements.at)
 * @license http://www.pimcore.org/license New BSD License
 */


 pimcore.registerNS("pimcore.plugin.magepim.product.tree");
 pimcore.plugin.magepim.product.tree = Class.create({
 
initialize: function() {
pimcore.layout.treepanelmanager.register('pimcore_panel_tree_products');

			// {{{
			// create drag enabled tree in the west
			var tree = new Ext.tree.TreePanel(
					{
						root : {
							text : 'root',
							id : 'pimcore_panel_tree_products',
							expanded : true,
							children : [
									{
										text : 'Child 1',
										data : 'Child 1 additional data',
										children : [
												{
													text : 'Child 1 Subchild 1',
													data : 'Some additional data of Child 1 Subchild 1',
													leaf : true
												},
												{
													text : 'Child 1 Subchild 2',
													data : 'Some additional data of Child 1 Subchild 2',
													leaf : true
												} ]
									},
									{
										text : 'Child 2',
										leaf : true,
										data : 'Last but not least. Data of Child 2'
									} ]
						},
						loader : new Ext.tree.TreeLoader({
							preloadChildren : true
						}),
						enableDrag : true,
						ddGroup : 't2div',
						region : 'west',
						title : 'Tree',
						layout : 'fit',
						width : 200,
						split : true,
						collapsible : true,
						autoScroll : true,
                                                parentPanel: Ext.getCmp("pimcore_panel_tree_left"),
						listeners : {
							startdrag : function() {
								var t = Ext.getCmp('target').body
										.child('div.drop-target');
								if (t) {
									t.applyStyles({
										'background-color' : '#f0f0f0'
									});
								}
							},
							enddrag : function() {
								var t = Ext.getCmp('target').body
										.child('div.drop-target');
								if (t) {
									t.applyStyles({
										'background-color' : 'white'
									});
								}
							}
						}
					});
			// }}}
			// {{{
			// create drop target in center
			var target = new Ext.Panel(
					{
						region : 'center',
						layout : 'fit',
						id : 'target',
						bodyStyle : 'font-size:13px',
						title : 'Drop Target',
						html : '<div class="drop-target" '
								+ 'style="border:1px silver solid;margin:20px;padding:8px;height:140px">'
								+ 'Drop a node here. I\'m the DropTarget.</div>'
						// setup drop target after we're rendered
						,
						afterRender : function() {
							Ext.Panel.prototype.afterRender.apply(this,
									arguments);
							this.dropTarget = this.body
									.child('div.drop-target');
							var dd = new Ext.dd.DropTarget(
									this.dropTarget,
									{
										// must be same as for tree
										ddGroup : 't2div'

										// what to do when user drops a node
										// here
										,
										notifyDrop : function(dd, e, node) {
											// var t =
											// Ext.getCmp('target').body;
											var msg = '<i>You have dropped node:</i><br><br><table style="font-size:13px">';
											msg += '<tr><td>id:</td><td><b>'
													+ node.node.id
													+ '</b></td></tr>'
											msg += '<tr><td>text:</td><td><b>'
													+ node.node.text
													+ '</b></td></tr>';
											msg += '<tr><td>leaf:</td><td><b>'
													+ node.node.leaf
													+ '</b></td></tr>';
											msg += '<tr><td>data:</td><td><b>'
													+ node.node.attributes.data
													+ '</b></td></tr>';
											msg += '</table>'
											Ext.getCmp('target').body.child(
													'div.drop-target').update(
													msg)
											return true;
										} // eo function notifyDrop
									});
						} // eo function afterRender
					}); // eo target
			// }}}
			// create and show the window
		
}
		}); // eo function onReady

 
 layoutProductTree = new pimcore.plugin.magepim.product.tree();
 pimcore.globalmanager.add("layout_product_tree", layoutProductTree);
 