<?php
class Magepim_Product_Resource extends Pimcore_Model_Resource_Abstract {

    protected $tableName = 'magepim_products';

    public function getById($id = null) {

        if ($id != null){
            $this->model->setId($id);
        }

        $data = $this->db->fetchRow('SELECT * FROM '.$this->tableName.' WHERE id = ?', $this->model->getId());

         if(!$data["id"]){
             return false;
         }
            /*throw new Exception("Object with the ID " . $this->model->getId() . " doesn't exists"); */


        $this->assignVariablesToModel($data);
    }
    
    public function getByEntityId($id = null) {
    
        if ($id != null){
            $this->model->setEntity_id($id);
        }
    
        $data = $this->db->fetchRow('SELECT * FROM '.$this->tableName.' WHERE entity_id = ?', $this->model->getEntity_id());
    
        if(!$data["id"])
            return false;
        /*throw new Exception("Object with the ID " . $this->model->getId() . " doesn't exists"); */
    
    
        $this->assignVariablesToModel($data);
    }
    
    public function getAllProducts(){
        $data  = $this->db->fetchAll('SELECT * FROM '.$this->tableName);
        return $data;
        
    }

    public function save() {

        $vars = get_object_vars($this->model);

        $buffer = array();

        $validColumns = $this->getValidTableColumns($this->tableName);

        if(count($vars))
            foreach ($vars as $k => $v) {

                if(!in_array($k, $validColumns))
                    continue;

                $getter = "get" . ucfirst($k);

                if(!is_callable(array($this->model, $getter)))
                    continue;

                $value = $this->model->$getter();

                if(is_bool($value))
                    $value = (int)$value;

                $buffer[$k] = $value;
            }

        if($this->model->getId() !== null) {
            $this->db->update($this->tableName, $buffer,$this->db->quoteInto("id = ?", $this->model->getId()));
            return;
        }

        $this->db->insert($this->tableName, $buffer);
        $this->model->setId($this->db->lastInsertId());
    }

    public function delete() {
        $this->db->delete($this->tableName, $this->db->quoteInto("id = ?", $this->model->getId()));
    }
        public function hasChilds() {
        $c = $this->db->fetchOne("SELECT id FROM ".$this->tableName ." WHERE parentId = ? LIMIT 1", $this->model->getId());
        return (bool) $c;
    }
      public function getChildAmount() {
        $c = $this->db->fetchOne("SELECT COUNT(*) AS count FROM ".$this->tableName." WHERE parentId = ?", $this->model->getId());
        return $c;
    }

}