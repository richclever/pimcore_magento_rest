<?php

/**
 * Soundcloud Consumer
 *
 * @author mk
 */
class Magepim_Consumer implements Magepim_Interface_Consumer{
	/**
	 *
	 * @var Zend_Http_Client
	 */
	protected $httpClient;
	
	/**
	 *
	 * @var OAuth_Interface_Consumer
	 */
	protected $consumer;
	/**
	 *
	 * @var Zend_Form
	 */
	protected $form;
	/**
	 *
	 * @var bool
	 */
	protected $hasAccess;
	/**
	 *
	 * @var string
	 */
	protected $name;
	
	public function __construct() {
	}
	/**
	 *
	 * @return Zend_Config
	 */
	public function getConfig() {
	    
	    return Magepim_Plugin::getConfig();
	    $callbackurl='http://pimcore.richard.com/plugin/magepim/admin/magento';
	    $config = array(
	            'callbackUrl' => $callbackurl,
	            'siteUrl' => 'http://magento.richard.com',	            
	            'authorizeUrl' => 'https://magento.richard.com/admin/oauth_authorize',
	            'accessTokenUrl'=> 'http://magento.richard.com/oauth/token',
	            'requestTokenUrl' => 'http://magento.richard.com/oauth/initiate?oauth_callback='.urlencode($callbackUrl)
	    );
	    
		return $config;
	}
	/**
	 *
	 * @param string $username
	 * @param bool $onlyPublic
	 */
	public function fetchTracks($username = null, $onlyPublic = true) {
		$tracks = array();
		$uri = sprintf(Soundcloud_Plugin::SOUNDCLOUD_API_USER_TRACKS, $username);		
		$this->httpClient->setUri($uri);
		$this->httpClient->setMethod(Zend_Http_Client::GET);
		if($onlyPublic) {
			$this->httpClient->setParameterGet('filter', 'public');
		}
		
		$response = $this->httpClient->request();
		if($response->isSuccessful()) {
			$xml = simplexml_load_string($response->getBody(), 'SimpleXMLElement', LIBXML_NOCDATA);			
			foreach($xml->track as $track) {
				$soundcloudTrack = new Soundcloud_Track($track);
				$soundcloudTrack->setOptions(array('color' => 'bc0000'));
				array_push($tracks, $soundcloudTrack);
			}
		}		
		return $tracks;
	}
	/**
	 *
	 */
	public function postTrack() {
		throw new Exception('Not implemented yet');
	}
	/**
	 *
	 */
	public function updateUserData() {
		throw new Exception('Not implemented yet');
	}
	/**
	 * 
	 */
	public function updateTrack() {
		throw new Exception('Not implemented yet');
	}
	/**
	 *
	 * @return Zend_Http_Client
	 */
	public function getHttpClient() {
		return $this->httpClient;
	}
	/**
	 *
	 * @param Zend_Http_Client $httpClient
	 * @return Soundcloud_Consumer
	 */
	public function setHttpClient($httpClient) {
		$this->httpClient = $httpClient;
		return $this;
	}
	
	/**
	 *
	 * @return OAuth_Interface_Consumer
	 */
	public function getConsumer() {
	    return $this->consumer;
	}
	/**
	 *
	 * @param OAuth_Interface_Consumer $consumer
	 */
	public function setConsumer($consumer) {
	    $this->consumer = $consumer;
	    return $this;
	}
	/**
	 *
	 * @return Zend_Form
	 */
	public function getForm() {
	    return $this->form;
	}
	/**
	 *
	 * @param Zend_Form $form
	 */
	public function setForm($form) {
	    $this->form = $form;
	    return $this;
	}
	/**
	 *
	 * @return bool
	 */
	public function getHasAccess() {
	    return $this->hasAccess;
	}
	/**
	 *
	 * @param bool $hasAccess
	 * @return OAuth_Consumer
	 */
	public function setHasAccess($hasAccess) {
	    $this->hasAccess = $hasAccess;
	    return $this;
	}
	/**
	 *
	 * @return string
	 */
	public function getName() {
	    return $this->name;
	}
	/**
	 *
	 * @param string $name
	 * @return OAuth_Consumer
	 */
	public function setName($name) {
	    $this->name = $name;
	    return $this;
	}
	

}

?>
