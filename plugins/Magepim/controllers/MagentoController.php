<?php
class Magepim_MagentoController extends Pimcore_Controller_Action_Admin
{
    
    protected $magepimConfig = null;
    public function init() {
        parent::init();
        $this->magepimConfig = Magepim_Plugin::getConfig();
        $this->httpClient = Magepim_Plugin::getHandler(new Magepim_Consumer())->getAccessHttpClient();
    }

    public function productsAction(){
        $consumer = 'Magepim_Consumer';
        
        
        try {
            /* @var $consumerModel OAuth_Interface_Consumer */
            $consumerModel = new $consumer();
            $oAuthHandler = new Magepim_Handler($consumerModel);
            $this->httpClient->setUri('https://magento.richard.com/api/rest/products');
            
            
            } catch(Exception $e) {
                $this->view->error = $e->getMessage();
            }
            $this->httpClient->setMethod(Zend_Http_Client::GET);
            $this->httpClient->setHeaders('Accept','application/json');
            $this->httpClient->setHeaders('Content-Type', 'application/json');
            $response = $this->httpClient->request();
            
            $result =  json_decode($response->getBody());
           
             foreach ($result as $productCounter =>  $productEntry) {
                $productArray= get_object_vars($productEntry); 
                $product[]=$productArray;
            }
            
            
            $totalCount = $this->getProductCount();
            return $this->_helper->json(array("products" => $product, "total" => $totalCount->catalog_size));
             
    } 
    
    public function getProductCount(){
        $consumer = 'Magepim_Consumer';
        try {
            /* @var $consumerModel OAuth_Interface_Consumer */
            $consumerModel = new $consumer();
            $oAuthHandler = new Magepim_Handler($consumerModel);
        
            $this->httpClient->setUri('https://magento.richard.com/api/rest/magepim/products/count');
        } catch(Exception $e) {
            $this->view->error = $e->getMessage();
        }
        $this->httpClient->setMethod(Zend_Http_Client::GET);
        $this->httpClient->setHeaders('Accept','application/json');
        $this->httpClient->setHeaders('Content-Type', 'application/json');
        $response = $this->httpClient->request();
        
        $result =  json_decode($response->getBody());
        return $result;
        
    }
  
    
}