pimcore.registerNS("pimcore.plugin.magepim");

pimcore.plugin.magepim = Class.create(pimcore.plugin.admin, {
    getClassName: function() {
        return "pimcore.plugin.magepim";
    },

    initialize: function() {
        pimcore.plugin.broker.registerPlugin(this);
    },
    checkboxRenderer: function (field, value, metaData, record, rowIndex, colIndex, store) {
        if(field == 'imported' && record.data.sku.length < 2) {
            return '';
        }
        metaData.css += ' x-grid3-check-col-td';
        return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', value ? '-on' : '');
    },
    importProduct : function(){
    	console.dir( magepim.grid.store);
    	var values = [];
    	var store = magepim.grid.getStore();
    	store.commitChanges();


    	
    	var records = store.getRange();
    	
        for (var i = 0; i < records.length; i++) {
            currentData = records[i];
            if (currentData) {
                    values.push(currentData.data);
            }
        }
        
        var data = Ext.encode(values);
    	

        //Ext.Ajax.addListener('requestcomplete', this.rechargeL,this);

        Ext.Ajax.request({
            url: '/plugin/Magepim/product/import',
            method:'POST',
            headers: {
                'action': 'import'
            },
            params: {
                data:data,
                xaction:"import"
            }
        });
    	
        
},
 
    pimcoreReady: function (params,broker){
        // add a sub-menu item under "Extras" in the main menu
        var toolbar = Ext.getCmp("pimcore_panel_toolbar");

        var action = new Ext.Action({
            id: "magepim_menu_item",
            text: "Magepim Magento products",
            iconCls:"fraud_check_menu_icon",
            handler: this.showTab
        });
        
  /*         var total = Ext.Ajax.request({
            url: '/plugin/Magepim/magento/count',
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                return obj.catalog_size;
                console.dir(obj);
             },
        });
        alert (total);*/

        toolbar.items.items[1].menu.add(action);
    },

    showTab: function() {
        magepim.panel = new Ext.Panel({
            id:         "spark_fraud_check_panel",
            title:      "Magepim Magento products",
            iconCls:    "spark_fraud_check_panel_icon",
            border:     false,
            layout:     "fit",
            closable:   true,
            items:       [magepim.getGrid()],
          
        });

        var tabPanel = Ext.getCmp("pimcore_panel_tabs");
        tabPanel.add(magepim.panel);
        tabPanel.activate("my_plugin_check_panel");

        pimcore.layout.refresh();
    },

   
    
    getGrid: function() {
    	var itemsPerPage = 20;
    	
        // fetch data from a webservice (which we haven't written yet!)
        magepim.store = new Ext.data.JsonStore({
        	type:				'ajax',
        	autoLoad: 			{params:{start: 0, limit: itemsPerPage}},
        	id:                 'magepim_store',
            url:                '/plugin/Magepim/magento/products',
            restful:            false,
            totalProperty:	'total',
            root:               "products",
            fields: [
                "entity_id",
                "sku",
                "type_id",
                "status",
                "price",
                "name",
                "short_description",
                "description"
            ]
        });
        
        
        
    
        
        magepim.store.load({
        		 params: {
        		        // specify params for the first page load if using paging
        		        start: 0,          
        		        limit: itemsPerPage,
        		        
        		    }});
       
        
        this.pagingtoolbar = new Ext.PagingToolbar({
            pageSize: itemsPerPage,
            total: 20,
            store: this.store,
            displayInfo: true,
            displayMsg: '{0} - {1} / {2}',
            emptyMsg: t("no_objects_found")
        });

        // add per-page selection
        this.pagingtoolbar.add("-");

        this.pagingtoolbar.add(new Ext.Toolbar.TextItem({
            text: t("items_per_page")
        }));
        this.pagingtoolbar.add(new Ext.form.ComboBox({
            store: [
                [10, "10"],
                [20, "20"],
                [50, "50"]
            ],
            mode: "local",
            width: 50,
            value: 20,
            triggerAction: "all",
            listeners: {
                select: function (box, rec, index) {
                    this.pagingtoolbar.pageSize = intval(rec.data.field1);
                    this.pagingtoolbar.moveFirst();
                }.bind(this)
            }
        }));

        var typeColumns = [
                new Ext.grid.CheckColumn({
                	header: t("imported"),
                    width: 40,
                    sortable: true,
                    menuDisabled: true,
                    dataIndex: "imported",
                    renderer: magepim.checkboxRenderer.bind(this, "imported")
                 }),
                {header: "Product ID",  width: 20, sortable: true, dataIndex: 'entity_id'},
                {header: "SKU", width: 20, sortable: true, dataIndex: 'sku'},
                {header: "Product Type", width: 100, sortable: true, dataIndex: 'type_id'},
                {header: "Status", width: 10, sortable: true, dataIndex: 'status'},
                {header: "Price", width: 20, sortable: false, dataIndex: 'price'},
                {header: "Product Name", width: 100, sortable: false, dataIndex: 'name'},
                {header: "Short Description", width: 100, sortable: false, dataIndex: 'short_description'},
                {header: "Description", width: 150, sortable: false, dataIndex:'description'},
        ];
        
        magepim.grid = new Ext.grid.EditorGridPanel({
                frame:          false,
                autoScroll:     true,
                store:          magepim.store,
                columns:        typeColumns,
                trackMouseOver: true,
                columnLines:    true,
                stripeRows:     true,
                bbar: this.pagingtoolbar,
                viewConfig:     { forceFit: true },
                buttons: [{
                    text: t("import"),
                    handler:magepim.importProduct,
                    scope:this
                }]
               
            });
        
        
        magepim.grid.getSelectionModel().on('rowselect', function(sm, rowIdx, r) {
        	
        	alert('selected');
            /*var detailPanel = Ext.getCmp('detailPanel');
            bookTpl.overwrite(detailPanel.body, r.data);*/
          });

       return magepim.grid;
    }
});

var magepim = new pimcore.plugin.magepim();




