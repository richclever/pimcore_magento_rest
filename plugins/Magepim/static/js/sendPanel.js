pimcore.registerNS("pimcore.plugin.magepim.sendPanel");

pimcore.plugin.magepim.sendPanel = Class.create({

    initialize: function(doc, type) {
        this.doc = doc;
        this.type = type;
    },

    getLayout: function(){
      if (this.layout == null) {

            this.layout = new Ext.Panel({
                title: t('sendpanel'),
                border: false,
                layout: "fit",
                iconCls: "send_panel_newsletter",
                items: [this.getLetters()]
            });
        }
        return this.layout;
    },

    hideWinLetter : function(){
    this.winLetter.destroy();
},

getFormLetter : function(){
    this.groupeSt = new Ext.data.JsonStore({
                            storeId:'groupeStore',
                            fields: ['type'],
                            root:'data',
                            url:"/plugin/Magepim/admin/getgroupes"
                        });
    this.groupeSt.load();

    this.formLetter = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        bodyStyle:'padding:5px 5px 0',
        items: [{
                    xtype:'textfield',
                    fieldLabel: t('subject of email'),
                    name: 'subject',
                    anchor:'95%'
                },  new Ext.form.ComboBox({
                        fieldLabel: t('group'),
                        hiddenName:'type',
                        store: this.groupeSt,
                        valueField:'type',
                        displayField:'type',
                        typeAhead: true,
                        mode: 'local',
                        triggerAction: 'all',
                        emptyText:'Sélectionnez un groupe',
                        selectOnFocus:true,
                        width:190
                    })
            ],

        buttons: [{
            text: t("send"),
            handler:this.createLetter,
            scope:this
        },"-",{
            text: t('cancel'),
            handler:this.hideWinLetter,
            scope:this
        }]
    });
    return this.formLetter;
},

rechargeL: function(conn,response,option){
    var resp = Ext.util.JSON.decode(response.responseText);
    
    Ext.Ajax.removeListener('requestcomplete', this.rechargeL,this);
    this.ngrid.store.reload();
    this.hideWinLetter();
    this.sendLetter(resp.data.id);

},

createLetter : function(){
        var val = this.formLetter.getForm().getFieldValues();

        var data = Ext.util.JSON.encode(val);
        Ext.Ajax.addListener('requestcomplete', this.rechargeL,this);

        Ext.Ajax.request({
            url: '/plugin/Magepim/admin/newsletter',
            method:'POST',
            headers: {
                'action': 'create'
            },
            params: {
                data:data,
                docid: this.doc.id,
                xaction:"create"
            }
        });
},
isSended : function(conn,response,option){//Not used

var resp = Ext.util.JSON.decode(response.responseText);
if(resp.sending > 0){
        Ext.Ajax.removeListener('requestcomplete', this.isSended,this);
        pimcore.helpers.showNotification(t('send complete'), t('the newsletter have been send to') + resp.sending + t(' emails'), "success");
        
}

},
sendLetter : function(id){
        this.sendWindow(id);
//        Ext.Ajax.addListener('requestcomplete', this.isSended,this);
//
//        Ext.Ajax.request({
//            url: '/plugin/Newsletter/admin/send',
//            method:'GET',
//            headers: {
//                'action': 'send'
//            },
//            params: {
//                id:id
//            }
//        });
},

openWinLetter : function(btn, ev) {
    this.doc.publish();
    var form = this.getFormLetter();

    this.winLetter = new Ext.Window({
        title:t('preparation of the sending'),
        layout:"fit",
        width:600,
        height:250,
        closeAction:'destroy',
        plain:true,
        modal:true,
        items: [form]
    });
        this.winLetter.show(this);

    },


   getLetters: function(){

        var expander = new Ext.ux.grid.RowExpander({
        tpl : new Ext.Template('<p><b>Sujet:</b><br/><div style="color:#666666;"> {subject}</div></p><br>',
            '<p><b>Texte:</b><br/><div style="color:#666666;"> {text}</div></p>')
    });
       
        var proxy = new Ext.data.HttpProxy({
            url: '/plugin/Newsletter/admin/newsletter'
        });



        var readerFields = [
        {
            name: 'id'
        },

        {
            name: 'subject'
        },

        {
            name: 'text'
        },

        {
            name: 'type'
        },

        {
            name: 'date'
        }
        ];
        var reader = new Ext.data.JsonReader({
            totalProperty: 'total',
            successProperty: 'success',
            root: 'data',
            idProperty: 'id'
        }, readerFields);

        var writer = new Ext.data.JsonWriter();
        // create the data store
        this.nstore = new Ext.data.Store({
            fields: readerFields,
            root:'data',
            restful: false,
            proxy: proxy,
            reader: reader,
            writer: writer,
            baseParams:{docid:this.doc.id}
        });

        // create the Grid
        this.ngrid = new Ext.grid.GridPanel({
            store: this.nstore,
            plugins: expander,            
            tbar:
            [{
                text: t('create a sending'),
                icon: '/pimcore/static/img/icon/newspaper_add.png',
                handler: this.openWinLetter,
                scope: this
            }],
            columns: [expander,
            {
                id       :'id',
                header   : 'id',
                width    : 75,
                sortable : true,
                dataIndex: 'id',
                hidden:true
            },
            {
                id:'subject',
                header   : t('title'),
                width    : 150,
                sortable : true,
                dataIndex: 'subject'
            },
            {
                header   : t('group'),
                width    : 100,
                sortable : true,
                dataIndex: 'type'
            },
            {
                header   : t('sending date'),
                width    : 150,
                sortable : true,
                dataIndex: 'date',
                renderer: function(d) {
                        var date = new Date(d * 1000);
                        return date.format("d/m/Y H:i:s");
                    }
            }
            ],
            stripeRows: true,
            autoExpandColumn: 'subject',
            height: 350,
            width: 600,
            title: t('newsletter'),
            // config options for stateful behavior
            id:'newsletterGrid'
        });

        this.nstore.load();
        return this.ngrid;
    },

    ///////////////////////////////////////////////////
    //Progressbar

     sendWindow: function (id) {
        this.sendId = id;
        this.errors = 0;
        this.enabled = true;

        this.progressBar = new Ext.ProgressBar({
            text: t('sending in progress')
        });

        this.window = new Ext.Window({
            title: t('newsletter sending'),
            layout:'fit',
            width:500,
            bodyStyle: "padding: 10px;",
            closeAction:'close',
            plain: true,
            modal: false,
            items: [this.progressBar]
        });

        this.window.show();
        this.window.on("close", function () {
            this.enabled = false;
        }.bind(this))

        window.setTimeout(this.progress.bind(this), 500);
    },

    progress : function(){
Ext.Ajax.request({
            url: "/plugin/Magepim/admin/send",
            params: {
                id:this.sendId
            },
            success: function (response) {
                var r = Ext.decode(response.responseText);

                try {
                    if (r.total) {                        
                        var status = 1-(r.waiting/r.total);
                        var sended = r.total-r.waiting;
                        this.progressBar.updateProgress(status, sended + "/" + r.total + "email envoyés");
                        if(status == 1){
                             pimcore.helpers.showNotification(t('send complete'), t('the newsletter have been send to') + r.total + t(' emails'), "success");

                        }else{
                            window.setTimeout(this.progress.bind(this), 100);
                        }
                        
                    }
                    else {
                        this.error();
                    }
                }
                catch (e) {
                    this.error();
                }
            }.bind(this)
        });
    },

    error: function(){

    }

});


